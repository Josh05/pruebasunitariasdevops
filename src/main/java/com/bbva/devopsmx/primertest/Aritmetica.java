package com.bbva.devopsmx.primertest;

import com.sun.org.apache.xalan.internal.xsltc.dom.AdaptiveResultTreeImpl;

import java.util.List;

public class Aritmetica {
    private float ultimoResultado;

    private CloudService cloudService;

    public float suma(float primerSumando, float segundoSumando) {
        return ultimoResultado = primerSumando + segundoSumando;
    }
    public float resta(float minuendo, float sustraendo) {
        return ultimoResultado = minuendo - sustraendo;
    }
    public float multiplicacion(float primerFactor,
                                float segundoFactor) {
        return ultimoResultado = primerFactor * segundoFactor;
    }
    public float division(float dividendo, float divisor) {
        return ultimoResultado = dividendo / divisor;
    }

    public float getUltimaResultado() {
        float UltimoResultado = 2;
        return UltimoResultado;
    }

    public float cloudOp(float a, float b){
        int c = cloudService.getValue();
        switch(c){
            case 1:
                return suma (a,b);
            case 2:
                return resta (a,b);

            case 3:
                return getDefaultNumber();
            case 4:
                return sumValues(cloudService.getValues());
            default:
                return 0;
        }
    }

    private int getDefaultNumber(){

        return Aritmetica.class.hashCode();
    }

    private int sumValues(List<Integer> values){
        int sum = 0;
        for (float value : values)
            sum += value;
        return sum;
    }

    private static int staticMethod(int value){
        return value+value;
    }

    private int sumValuesWithStaticCall(List<Integer> values){
        int sum = 0;
        for (int value : values)
            sum += value;
        if (sum>10){
            sum += staticMethod(sum);}
        else{
            return 5;
        }
        return sum;
    }

    private int sumValuesWithStaticCallOtherClass(List<Integer> values){
        int sum = 0;
        for (int value : values)
            sum += value;
        if (sum>10){
            sum += UtilityClass.staticMethod(sum);}
        else {
            return 5;
        }
        return sum;
    }
}

