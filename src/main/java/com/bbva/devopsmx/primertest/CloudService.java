package com.bbva.devopsmx.primertest;

import java.util.List;

public interface CloudService {
    int getValue();

    List<Integer> getValues();
}
