package com.bbva.devopsmx.primertest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareEverythingForTest;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;


import java.text.DateFormat;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.verifyPrivate;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Aritmetica.class, UtilityClass.class})

public class AritmeticaTest {

    @InjectMocks

    Aritmetica aritmetica;

    @Mock
    CloudService cloudServiceMock;

    @Before
    public void beforeEachTest() {
        aritmetica = PowerMockito.spy(new Aritmetica());
        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void testSuma() {
        float expectedValue = 2;
        float primerSumando = 1;
        float segundoSumando = 1;
        float delta = 0;
        float value = aritmetica.suma(primerSumando, segundoSumando);
        assertEquals(expectedValue, value, delta);

    }

    @Test
    public void testResta() {
        float expectedValue2 = 2;
        float minuendo = 3;
        float sustraendo = 1;
        float delta2 = 0;
        float value2 = aritmetica.resta(minuendo, sustraendo);
        assertEquals(expectedValue2, value2, delta2);
    }

    @Test
    public void testDivision() {
        float expectedValue3 = 3;
        float dividendo = 3;
        float divisor = 1;
        float delta3 = 0;
        float value3 = aritmetica.division(dividendo, divisor);
        assertEquals(expectedValue3, value3, delta3);
    }

    @Test
    public void testMultiplicacion() {
        float expectedValue4 = 3;
        float primerFactor = 3;
        float segundoFactor = 1;
        float delta4 = 0;
        float value4 = aritmetica.multiplicacion(primerFactor, segundoFactor);
        assertEquals(expectedValue4, value4, delta4);
    }

    @Test
    public void testCloudOpVal1() {
        when(cloudServiceMock.getValue()).thenReturn(1);
        assertEquals(4, aritmetica.cloudOp(3, 1), 0);
    }

    @Test
    public void testCloudOpVal2() {
        when(cloudServiceMock.getValue()).thenReturn(2);
        assertEquals(2, aritmetica.cloudOp(3, 1), 0);
    }

    @Test
    public void testCloudOpSpy() {
        int getServiceValueMock = 1;
        float anyNumber = 0;
        float val1 = 16;
        float val2 = 28;
        int wantedNumberOfInvocations = 1;
        when(cloudServiceMock.getValue()).thenReturn(getServiceValueMock);
        when(aritmetica.suma(anyFloat(), anyFloat())).thenReturn(anyNumber);
        aritmetica.cloudOp(val1, val2);
        verify(aritmetica, times(wantedNumberOfInvocations)).suma(anyFloat(), anyFloat());
    }

    //Testing a private method
    @Test
    public void testGetDefaultNumber() throws Exception {
        int expectedValue = Aritmetica.class.hashCode();
        int value = Whitebox.invokeMethod(aritmetica, "getDefaultNumber");
        assertEquals(expectedValue, value);
    }

    //Testing a private method with params
    @Test
    public void testSumValues() throws Exception {
        int expectedValue = 10;
        List<Integer> listParamMock = Arrays.asList(5, 5);
        int value = Whitebox.invokeMethod(aritmetica, "sumValues", listParamMock);
        assertEquals(expectedValue, value);
    }

    //Mocking a privat method
    @Test
    public void testCloudOpVal3() throws Exception {
        int getServiceValueMock = 3;
        float expectedValue = 6;
        float val1 = 16;
        float val2 = 28;
        int wantedNumberOfInvocations = 1;
        when(cloudServiceMock.getValue()).thenReturn(getServiceValueMock);
        PowerMockito.when(aritmetica, "getDefaultNumber").thenReturn(6);
        float value = aritmetica.cloudOp(val1, val2);
        assertEquals(expectedValue, value, 0);
        verifyPrivate(aritmetica, times(wantedNumberOfInvocations)).invoke("getDefaultNumber");
    }

    //Mocking a privat method with params
    @Test
    public void testCloudOpVal4() throws Exception {
        int getServiceValueMock = 4;
        float expectedValue = 9;
        float val1 = 16;
        float val2 = 28;
        int wantedNumberOfInvocations = 1;
        List<Integer> listParamMock = Arrays.asList(3, 3);
        when(cloudServiceMock.getValue()).thenReturn(getServiceValueMock);
        when(cloudServiceMock.getValues()).thenReturn(listParamMock);
        PowerMockito.doReturn(9).when(aritmetica, "sumValues", any());
        PowerMockito.when(aritmetica, "getDefaultNumber").thenReturn(6);
        float value = aritmetica.cloudOp(val1, val2);
        assertEquals(expectedValue, value, 0);
        verifyPrivate(aritmetica, times(wantedNumberOfInvocations)).invoke("sumValues", listParamMock);
    }

    @Test
    public void testStaticMethod() throws Exception {
        int paramValue = 4;
        int expectedValue = 8;
        int value = Whitebox.invokeMethod(Aritmetica.class, "staticMethod", paramValue);
        assertEquals(expectedValue, value);

    }

    @Test
    public void testSumValuesWithStaticCall() throws Exception {
        int valueMock = 10;
        int expectedValue = 40;
        List<Integer> listParamMock = Arrays.asList(20, 10);
        PowerMockito.mockStatic(Aritmetica.class);
        PowerMockito.doReturn(valueMock).when(Aritmetica.class, "staticMethod", anyInt());
        int value = Whitebox.invokeMethod(aritmetica, "sumValuesWithStaticCall", listParamMock);
        assertEquals(expectedValue, value);

    }

    @Test
    public void testSumValuesless10WithStaticCall() throws Exception {
        int valueMock = 10;
        int expectedValue = 5;
        List<Integer> listParamMock = Arrays.asList(0, 5);
        PowerMockito.mockStatic(Aritmetica.class);
        PowerMockito.doReturn(valueMock).when(Aritmetica.class, "staticMethod", anyInt());
        int value = Whitebox.invokeMethod(aritmetica, "sumValuesWithStaticCall", listParamMock);
        assertEquals(expectedValue, value);

    }

    @Test
    public void testSumValuesWithStaticCallOtherClass() throws Exception {
        int valueMock = 10;
        int expectedValue = 30;
        int wantedNumberOfInvocations = 1;
        List<Integer> listParamMock = Arrays.asList(15, 5);

        PowerMockito.mockStatic(UtilityClass.class);
        PowerMockito.doReturn(valueMock).when(UtilityClass.class, "staticMethod", anyInt());

        int value = Whitebox.invokeMethod(aritmetica, "sumValuesWithStaticCallOtherClass", listParamMock);
        assertEquals(expectedValue, value);

        PowerMockito.verifyStatic(times(wantedNumberOfInvocations));
        UtilityClass.staticMethod(20);

    }

    @Test
    public void testCloudOpValDefault() throws Exception {
        int getServiceValueMock = 5;
        float anyNumber = 0;
        float val1 = 16;
        float val2 = 28;
        int wantedNumberOfInvocations = 1;
        when(cloudServiceMock.getValue()).thenReturn(getServiceValueMock);
        aritmetica.cloudOp(val1, val2);
    }

    @Test
    public void testgetUltimoResultado() throws Exception {
        float ultimaResultado = 2;
        float delta5 = 0;
        PowerMockito.mockStatic(Aritmetica.class);
        float value = Whitebox.invokeMethod(aritmetica, "getUltimaResultado");
        assertEquals(ultimaResultado, value, delta5);

    }

}