package com.bbva.devopsmx;


import org.junit.*;



import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import static org.junit.Assert.*;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    private static final Logger logger = Logger.getLogger("Logger");
    @BeforeClass
    public static void setUpClass() {
        logger.info("Before all test");
    }
    @AfterClass
    public static void tearDownClass() {
        logger.info("After all tests");
    }
    @Before
    public void setUpTest() {
        logger.info("Before each test");
    }
    @After
    public void tearDownTest() {
        logger.info("After each test");
    }
    @Test
    public void test1() {
        logger.info("Test 1");
    }
    @Test
    public void test2() {
        logger.info("Test 2");
    }
    @Test
    public void getUserTest() {
        List<String> users = Arrays.asList(new String[]{"a1", "a2"});
        String id = "a3";
        assertNull(new App().getUser(users, id));
    }

    @Test
    public void getUserNotNullTest() {
        List<String> users = Arrays.asList(new String[]{"a1", "a2"});
        String id = "a2";
        assertNotNull(new App().getUser(users, id));
    }

    @Test
    public void assertSameTest() {
        List<String> list1 = Arrays.asList(new String[]{"a1", "a2"});
        List<String> list2 = Arrays.asList(new String[]{"a1", "a2"});
        List<String> list3 = list1;
        //assertSame(list1, list2);
        //assertTrue(list1.equals(list2));
        //assertTrue(condition: list1==list3);
        assertSame(list1,list3);
    }

    @Test
    public void assertNotSameTest() {
        List<String> list1 = Arrays.asList(new String[]{"a1", "a2"});
        List<String> list2 = Arrays.asList(new String[]{"a1", "a2"});
        List<String> list3 = list1;
        //assertSame(list1, list2);
        //assertTrue(list1.equals(list2));
        //assertTrue(condition: list1==list3);
        assertNotSame(list1, list2);
    }

    @Test
    public void getHelloMessage() {
        String ExpectedMessage = null;
        assertSame(ExpectedMessage,App.main(null));


    }


}
